I = imread('example.jpeg');

starttime = time;
palette = autopalette_easy(I, 'reorder', 'none', 'colorspace', 'hsl');
endtime = time;
printf('execution time: %f\n', endtime - starttime);
figure 1
clf
subplot(2, 3, 1:2), imshow(I)
subplot(2, 3,   3), imshow(reshape(palette, [], 1, 3))
subplot(2, 3, 4:6), grid on, hold on
multiplier = 1 + all(strcmp(class(palette(1,1)), 'uint8')) * 254;
axis([0 1 0 1 0 1] .* multiplier)
xlabel("R"), ylabel("G"), zlabel("B")
legend_labels = cell(size(palette, 1), 1);
legend_handles = zeros(size(palette, 1), 1);
for i = 1:size(palette, 1)
    c = palette(i, :);
    if all(strcmp(class(c), 'double'))
        c = uint8(c * 255);
    endif
    printf('\x1b[48;2;%d;%d;%dm  \x1b[0m %d, %d, %d\n',
        c(1), c(2), c(3), palette(i,1), palette(i,2), palette(i,3));
    printf('\x1b[48;2;%d;%d;%dm  \x1b[0m #%02X%02X%02X\n',
        c(1), c(2), c(3), palette(i,1), palette(i,2), palette(i,3));
    legend_handles(i) = plot3(palette(i,1), palette(i,2), palette(i,3), 's',
        'color', double(palette(i,:)) ./ multiplier, 'markerfacecolor', 'auto');
    plot3([palette(i,1) palette(i,1)], [palette(i,2) palette(i,2)], [palette(i,3) 0],
        'color', double(palette(i,:)) ./ multiplier)
    text(palette(i,1) + 5, palette(i,2), palette(i,3), num2str(i))
    legend_labels{i} = sprintf('%d: #%02X%02X%02X', i, c(1), c(2), c(3));
endfor
view(3)
legend(legend_handles, legend_labels)
hold off
