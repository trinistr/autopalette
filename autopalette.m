function [v, freq, weight] = autopalette (img, targets, color_levels = [30 5 25])
%-- autopalette(IMAGE, TARGETS)
%-- autopalette(..., COLOR_LEVELS)
%-- [COLORS, FREQS, WEIGHTS] = autopalette(...)
%   Generate a palette of colors from IMAGE according to TARGETS.
%
%   This function uses the image package, which must be loaded already.
%
%   IMAGE is a color image with its components in [0 1] range.
%   TARGETS is a matrix where rows represent individual color targets and
%   columns define search values as follows for e.g. HSL:
%       [   target saturation, min saturation, max saturation,...
%           target lightness, min lightness, max lightness,...
%           saturation weight, lightness weight, frequency weight   ].
%   Weights control how likely a specific color is to be selected. For color
%   components this is based on distance from target value, for frequency —
%   on number of occurences compared to maximum number across all colors.
%   Returned COLORS is a matrix whose rows correspond to targets and columns
%   are color components.
%
%   If the optional argument COLOR_LEVELS is given, it must be a vector of
%   3 elements giving numbers of levels for component quantization.
%   Default: [30 5 25].
%
%   If FREQS is requested, it is a column vector with number of occurrences
%   of each color in COLORS.
%
%   If WEIGHTS is requested, it is a column vector with computed weights of
%   colors in COLORS. These weights represent how close the colors are to
%   their targets.
%
%   Copyright © 2019, 2020 Aleksandr Bulantcov

    % flatten the image into a vector of pixels
    img = reshape(img, [], 3);

    % quantize colors
    comp1_points = color_levels(1) + 1;
    comp1_space = linspace(0, 1, comp1_points);
    comp2_points = color_levels(2) + 1;
    comp2_space = linspace(0, 1, comp2_points);
    comp3_points = color_levels(3) + 1;
    comp3_space = linspace(0, 1, comp3_points);
    img = [
        imquantize(img(:, 1), comp1_space(2:end), comp1_space + 1/(2 * comp1_points))'...
        imquantize(img(:, 2), comp2_space(2:end), comp2_space + 1/(2 * comp2_points))'...
        imquantize(img(:, 3), comp3_space(2:end), comp3_space + 1/(2 * comp3_points))' ];

    % find unique colors and count them
    [colors, ~, colors_j] = unique(img, 'rows');
    % combine into one matrix for ease of use
    colors = [colors accumarray(colors_j, 1)];
    colors_nmax_log = log(max(colors(:, 4)));

    v = -ones(size(targets, 1), 3);
    if isargout(2)
        freq = zeros(size(targets, 1), 1);
    end
    if isargout(3)
        weight = zeros(size(targets, 1), 1);
    end

    % find colors
    for target_i = 1:size(targets, 1)
        target = targets(target_i, :);
        % find colors that satisfy the requirements
        % note: find() is used to generate a vector with correct size
        valid_color_indices = find(
            colors(:,2) >= target(2) & colors(:,2) <= target(3) & ...
            colors(:,3) >= target(5) & colors(:,3) <= target(6) & ...
            !ismember(colors(:,1:3), v, 'rows'));
        % select valid colors, add a column for weight
        valid_colors = [
            colors(valid_color_indices, :), zeros(length(valid_color_indices), 1)
        ];
        % compute the weight
        valid_colors(:,5) = ...
            ( (1 - abs(valid_colors(:,2) - target(1))) * target(7) +
              (1 - abs(valid_colors(:,3) - target(4))) * target(8) +
              log(valid_colors(:,4)) / colors_nmax_log * target(9)
            ) / sum(target(7:9));
        % select the best color(s)
        final_colors = valid_colors(
            valid_colors(:,5) == max(valid_colors(:,5)), :);
        % set return values
        if size(final_colors, 1) != 0
            v(target_i,:) = final_colors(1,1:3);
            if isargout(2)
                freq(target_i) = final_colors(1,4);
            end
            if isargout(3)
                weight(target_i) = final_colors(1,5);
            end
        else
            warning('could not find color for target #%d', target_i)
        end
    end
end
