function hsl = rgb2hsl (rgb)
% -- HSL_IMG = rgb2hsl (RGB_IMG)
%     Transform an image from RGB to HSL color space.
%
%     A color in HSL space is represented by hue, saturation and lightness
%     levels in a cylindrical coordinate system.  Hue is the azimuth and
%     describes the dominant color.  Saturation is the radial distance and
%     gives the amount of hue mixed into the color.  Lightness is the height
%     and is the amount of white mixed into the color.
%
%     Input is expected to be of class 'double'.
%
%     Output size will be the same as input.
%     Output will be of class 'double'.
%
%     See also: hsl2rgb.

    H = rgb2hsv(rgb)(:,:,1);
    M = max(rgb, [], 3);
    m = min(rgb, [], 3);
    L = (M + m) / 2;
    S = (M - m) ./ (1 - abs(2*L - 1));
    S(L == 1 | L == 0) = 0;
    hsl = cat(3, H, S, L);
end
