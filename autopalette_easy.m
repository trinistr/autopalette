function v = autopalette_easy (img, varargin)
%-- autopalette_easy(IMAGE)
%-- autopalette_easy(..., PROPERTY, VALUE)
%   Generate a palette of colors from an image.
%
%   Try to extract 6 colors from the image, which can be defined as:
%       medium vibrant, light vibrant, dark vibrant,
%       medium muted, light muted, dark muted.
%   Depending on the colors present in the image and passed options, not
%   all of 6 colors may be returned.
%
%   IMAGE must be a color image in RGB colorspace. Returned value is
%   a matrix with rows being the extracted colors in RGB space.
%
%   Additional property-value pairs can be specified to control how
%   colors are returned. Following properties are supported:
%       'complete': true|false
%           Generate colors which could not be found in the image by
%           modifying other colors. Default is false.
%       'reorder': 'group'|'all'|'none'
%           Reorder colors based on their dominance in the image.
%           'group' can put muted color group first if they are more
%           prominent in the image, 'all' reorders all colors and 'none'
%           disables reordering. Default is 'none'.
%       'cull': true|false
%           Remove rows for colors which could not be found from the
%           returned array. Color generation takes precedence over this
%           option. Default is true.
%       'colorspace': 'hsv'|'hsl'|'hsi'
%           Select which colorspace to use for extraction. Results may
%           be different when using different colorspaces. HSV is somewhat
%           faster than HSL or HSI, but may give worse results. HSL and HSI
%           require functions for conversion to be present. Default is 'hsl'.
%
%   See also: autopalette.
%
%   Copyright © 2019, 2020 Aleksandr Bulantcov

    pkg load image;

    [~, fill_missing, reorder, remove_missing, space] = parseparams(varargin,
        'complete', false,
        'reorder', 'none',
        'cull', true,
        'colorspace', 'hsl');

    % color targets
    t = zeros([6, 9]);
    % number of levels of quantization
    levels = zeros([1, 3]);
    switch space
        case 'hsl'
            t(:, 1:6) = [
              %    saturation      lightness
              % target min max
                0.90 0.55 1.00  0.45 0.30 0.70 % vibrant normal
                0.95 0.50 1.00  0.69 0.55 1.00 % vibrant light
                0.85 0.50 1.00  0.21 0.00 0.45 % vibrant dark
                0.30 0.00 0.40  0.45 0.30 0.70 % muted normal
                0.35 0.00 0.40  0.69 0.55 1.00 % muted light
                0.25 0.00 0.40  0.21 0.00 0.45 % muted dark
            ];
            t(:, 7:9) = repmat([3 6 1], size(t, 1), 1);
            levels = [30 10 20];
            rgb2smth = @rgb2hsl;
            smth2rgb = @hsl2rgb;
        case 'hsi'
            t(:, 1:6) = [
              %    saturation      intensity
              % target min max
                1.00 0.45 1.00  0.50 0.30 0.70 % vibrant normal
                1.00 0.45 1.00  0.74 0.55 1.00 % vibrant light
                1.00 0.45 1.00  0.26 0.00 0.45 % vibrant dark
                0.30 0.00 0.40  0.50 0.30 0.70 % muted normal
                0.30 0.00 0.40  0.74 0.55 1.00 % muted light
                0.30 0.00 0.40  0.26 0.00 0.45 % muted dark
            ];
            t(:, 7:9) = repmat([3 6 1], size(t, 1), 1);
            levels = [30 10 20];
            rgb2smth = @rgb2hsi;
            smth2rgb = @hsi2rgb;
        case 'hsv'
            t(:, 1:6) = [
              %    saturation       value
              % target min max
                0.95 0.38 1.00  0.86 0.47 1.00 % vibrant normal
                0.60 0.11 0.90  0.98 0.78 1.00 % vibrant light
                0.92 0.67 1.00  0.39 0.15 0.90 % vibrant dark
                0.46 0.00 0.57  0.59 0.30 0.82 % muted normal
                0.27 0.00 0.49  0.80 0.55 0.94 % muted light
                0.40 0.00 0.57  0.26 0.10 0.63 % muted dark
            ];
            t(:, 7:9) = repmat([6 3 1], size(t, 1), 1);
            levels = [30 13 13];
            rgb2smth = @rgb2hsv;
            smth2rgb = @hsv2rgb;
    end

    [v, frequencies, ~] = autopalette(rgb2smth(im2double(img)), t, levels);

    switch reorder
        case 'group'
            if 1.3 * sum(frequencies(1:3)) * 3/nnz(frequencies(1:3) >= 0)...
                    < sum(frequencies(4:6)) * 3/nnz(frequencies(4:6) >= 0)
                v = [v(4:6, :); v(1:3, :)];
            end
        case 'all'
            v = sortrows([v frequencies], [-4])(:, 1:3);
    end

    % try to fill missing colors
    if fill_missing
        if all(v(:) < 0)
            error('not a single color was found')
        end
        v2 = zeros(size(v));
        % vibrant normal
        if v(1, 1) < 0
            if v(3, 1) >= 0
                v2(1, :) = [v(3, 1:2) t(1, 4)];
            elseif v(2, 1) >= 0
                v2(1, :) = [v(2, 1:2) t(1, 4)];
            else % no vibrant colors
                warning('no vibrant colors found')
                if v(4, 1) >= 0
                    v2(1, :) = [v(4, 1) t(1, 1) v(4, 3)];
                elseif v(5, 1) >= 0
                    v2(1, :) = [v(5, 1) t(1, 1) t(1, 4)];
                elseif v(6, 1) >= 0
                    v2(1, :) = [v(6, 1) t(1, 1) t(1, 4)];
                end
            end
        end
        % vibrant light
        if v(2, 1) < 0
            if v(1, 1) >= 0
                v2(2, :) = [v(1, 1:2) t(2, 4)];
            elseif v(3, 1) >= 0
                v2(2, :) = [v(3, 1:2) t(2, 4)];
            else % no vibrant colors
                warning('no vibrant colors found')
                if v(5, 1) >= 0
                    v2(2, :) = [v(5, 1) t(2, 1) v(5, 3)];
                elseif v(4, 1) >= 0
                    v2(2, :) = [v(4, 1) t(2, 1) t(2, 4)];
                elseif v(6, 1) >= 0
                    v2(2, :) = [v(6, 1) t(2, 1) t(2, 4)];
                end
            end
        end
        % vibrant dark
        if v(3, 1) < 0
            if v(1, 1) >= 0
                v2(3, :) = [v(1, 1:2) t(3, 4)];
            elseif v(2, 1) >= 0
                v2(3, :) = [v(2, 1:2) t(3, 4)];
            else % no vibrant colors
                warning('no vibrant colors found')
                if v(6, 1) >= 0
                    v2(3, :) = [v(6, 1) t(3, 1) v(6, 3)];
                elseif v(4, 1) >= 0
                    v2(3, :) = [v(4, 1) t(3, 1) t(3, 4)];
                elseif v(5, 1) >= 0
                    v2(3, :) = [v(5, 1) t(3, 1) t(3, 4)];
                end
            end
        end
        % muted normal
        if v(4, 1) < 0
            if v(6, 1) >= 0
                v2(4, :) = [v(6, 1:2) t(4, 4)];
            elseif v(5, 1) >= 0
                v2(4, :) = [v(5, 1:2) t(4, 4)];
            else % no muted colors
                warning('no muted colors found')
                if v(1, 1) >= 0
                    v2(4, :) = [v(1, 1) t(4, 1) v(1, 3)];
                elseif v(3, 1) >= 0
                    v2(4, :) = [v(3, 1) t(4, 1) t(4, 4)];
                elseif v(2, 1) >= 0
                    v2(4, :) = [v(2, 1) t(4, 1) t(4, 4)];
                end
            end
        end
        % muted light
        if v(5, 1) < 0
            if v(4, 1) >= 0
                v2(5, :) = [v(4, 1:2) t(5, 4)];
            elseif v(6, 1) >= 0
                v2(5, :) = [v(6, 1:2) t(5, 4)];
            else % no muted colors
                warning('no muted colors found')
                if v(2, 1) >= 0
                    v2(5, :) = [v(2, 1) t(5, 1) v(2, 3)];
                elseif v(1, 1) >= 0
                    v2(5, :) = [v(1, 1) t(5, 1) t(5, 4)];
                elseif v(3, 1) >= 0
                    v2(5, :) = [v(3, 1) t(5, 1) t(5, 4)];
                end
            end
        end
        % muted dark
        if v(6, 1) < 0
            if v(4, 1) >= 0
                v2(6, :) = [v(6, 1:2) t(6, 4)];
            elseif v(5, 1) >= 0
                v2(6, :) = [v(5, 1:2) t(6, 4)];
            else % no muted colors
                warning('no muted colors found')
                if v(3, 1) >= 0
                    v2(6, :) = [v(3, 1) t(6, 1) v(3, 3)];
                elseif v(1, 1) >= 0
                    v2(6, :) = [v(1, 1) t(6, 1) t(6, 4)];
                elseif v(2, 1) >= 0
                    v2(6, :) = [v(2, 1) t(6, 1) t(6, 4)];
                end
            end
        end
        v(v < 0) = v2(v < 0);
    % or remove missing colors
    elseif remove_missing
        v = v(v(:,1) >= 0, :);
    end

    v = reshape(v, [], 1, 3);
    v = smth2rgb(v);
    v = reshape(v, [], 3);
    if strcmp(class(img), 'uint8')
        v = uint8(v * 255);
    end
end
