#!/usr/bin/octave -qf

I = imread(argv(){1});

warning('off');
addpath(fileparts(make_absolute_filename(program_invocation_name)));

palette = autopalette_easy(I, 'reorder', 'none', 'complete', true, 'colorspace' ,'hsl');
for i = 1:size(palette, 1)
    printf('%02X%02X%02X\n', palette(i,1), palette(i,2), palette(i,3));
endfor
