# Autopalette

Autopalette is a set of Octave functions for extracting colors with desired
characteristics from images.

Extraction is done by searching for colors closest to target saturation /
luminance in HSx color space (hue is ignored) with frequency taken into
account too. Note that this search is not intelligent and does not consider
contrast or eye-catchiness of image elements.

## Installing

If you intend to use these functions from other Octave programs, they need to
be placed somewhere in the search path. Run `path` to determine which
directories are suitable, or use `addpath` to add a directory. Otherwise,
just place the downloaded / cloned repository anywhere convenient.

Following function and script files are included:
- `autopalette.m`, `autopalette_easy.m` — the actual functions for extracting
  colors;
- `rgb2hsl.m`, `hsl2rgb.m` — functions for conversion to and from HSL
  colorspace, used in `autopalette_easy` function if HSL is requested;
- `autopalette_test.m` — an example script showing usage of Autopalette;
- `autopalette_runner.m` — a command line script for use outside Octave.

## Usage

Autopalette provides two interfaces: `autopalette` is the low-level interface,
and `autopalette_easy` is the high-level one.

- `autopalette_easy` is intended to be used for cases like automatic desktop
  theme creation from wallpaper image. It uses six predefined color targets,
  with function parameters controlling behavior in edge cases and colorspace
  selection. The image parameter must be an RGB color image 3D matrix.
  Basic usage:
  ```
  >> autopalette_easy(imread('example.jpeg'))
  ans =

    235   29    7
    250  108   93
    105   18    9
     79  154  163
    143  184  201
     43   71   68
  ```
  Run `help autopalette_easy` for documentation.

- `autopalette` provides control over individual color targets. User can specify
  any number of color targets with different constraints and weights, while also
  controlling number of quantization levels. Passed image parameter must be
  already converted to HSx, and 'image' package must be loaded.
  Basic usage:
  ```
  >> pkg load image;
  >> I = rgb2hsl(im2double(imread('example.jpeg')));
  >> target = [0.90 0.55 1.00  0.45 0.30 0.70  3 6 1];
  >> autopalette(I, target)
  ans =

     0.98280   0.88333   0.45923
  ```
  Number of returned colors and their order will be exactly the same as targets.
  If the image did not contain a color for some target, its row will contain
  negative values.
  Run `help autopalette` for documentation.

For command line usage `autopalette_runner.m` script is included. This script
will work if kept in the same directory as Autopalette functions or if they
are available through search path. The script runs `autopalette_easy` on the
file argument and prints results in hexadecimal form.
Usage:
```
$ ./autopalette_runner.m example.jpeg
EB1D07
FA6C5D
691209
4F9AA3
8FB8C9
2B4744
```

## License

Autopalette, Copyright © 2019, 2020 Aleksandr Bulantcov

This software is distributed under the terms of Mozilla Public License 2.0.
See LICENSE for more information.

Example image (`example.jpeg`) by [Kevin Grieve][] at [Unsplash][] and is
distributed under the terms of [Unsplash license][].

[Kevin Grieve]: https://unsplash.com/photos/NYFFC30zbv0
[Unsplash]: https://unsplash.com
[Unsplash license]: https://unsplash.com/license
