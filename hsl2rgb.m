function rgb = hsl2rgb (hsl)
% -- RGB_IMG = hsl2rgb (HSL_IMG)
%     Transform an image from HSL to RGB color space.
%
%     A color in HSL space is represented by hue, saturation and lightness
%     levels in a cylindrical coordinate system.  Hue is the azimuth and
%     describes the dominant color.  Saturation is the radial distance and
%     gives the amount of hue mixed into the color.  Lightness is the height
%     and is the amount of white mixed into the color.
%
%     Input is expected to be of class 'double'.
%
%     Output size will be the same as input.
%     Output will be of class 'double'.
%
%     See also: rgb2hsl.

    C  = (1 - abs(2*hsl(:,:,3) - 1)) .* hsl(:,:,2);
    X = C .* (1 - abs(mod(hsl(:,:,1)*6, 2) - 1));
    range1 = hsl(:,:,1) < 1/6;
    range2 = 1/6 <= hsl(:,:,1) & hsl(:,:,1) < 2/6;
    range3 = 2/6 <= hsl(:,:,1) & hsl(:,:,1) < 3/6;
    range4 = 3/6 <= hsl(:,:,1) & hsl(:,:,1) < 4/6;
    range5 = 4/6 <= hsl(:,:,1) & hsl(:,:,1) < 5/6;
    range6 = 5/6 <= hsl(:,:,1);
    R = G = B = zeros(size(hsl)(1:2));
    R(range1 | range6) = C(range1 | range6);
    R(range2 | range5) = X(range2 | range5);
    G(range2 | range3) = C(range2 | range3);
    G(range1 | range4) = X(range1 | range4);
    B(range4 | range5) = C(range4 | range5);
    B(range3 | range6) = X(range3 | range6);
    m = hsl(:,:,3) - C/2;
    R += m;
    G += m;
    B += m;
    rgb = cat(3, R, G, B);
end
